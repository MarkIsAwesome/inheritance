public class BookStore {
    public static void main(String[] args) {
        Book[] fB = new Book[5];
        fB[0] = new Book("Letting Go", "Philip Roth");
        fB[1] = new ElectronicBook("Fear of flying", "Erica Jong", 10);
        fB[2] = new Book("When Breath Becomes Air", "Paul Kalanithi");
        fB[3] = new ElectronicBook("The Adventures of Tom Sawyer", "Mark Twain", 12);
        fB[4] = new ElectronicBook("In the Heart of the Sea", "Nathaniel Philbrick", 10);

        for (int i = 0; i < fB.length;i++) {
            System.out.println(fB[i]);
        }

        
    }
}
