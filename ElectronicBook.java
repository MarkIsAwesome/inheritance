public class ElectronicBook extends Book{
    private double numberBytes;


    public ElectronicBook(String title, String author, double numberBytes) {
        super(title,author);
        this.numberBytes = numberBytes;
        
    }
    public double getNumberBytes() {
        return this.numberBytes;
    }
    public String toString() {
        String fromSuper = super.toString();

        return fromSuper + "\n" +"The numberBytes is: " +this.numberBytes + " MB";
    }

}
